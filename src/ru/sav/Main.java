package ru.sav;

public class Main {

    public static void main(String[] args) {
        Backend backend = new Backend(1_000L);
        Frontend frontend = new Frontend();

        Thread processorThread1 = new Thread(new Processor("��������� 1", backend, frontend));
        processorThread1.setName("processorThread1");
        processorThread1.setDaemon(true);
        processorThread1.start();

        Thread processorThread2 = new Thread(new Processor("��������� 2", backend, frontend));
        processorThread2.setName("processorThread2");
        processorThread2.setDaemon(true);
        processorThread2.start();

        Thread clientThread1 = new Thread(new Client("������ 1", frontend, RequestType.TOPUP, 100L));
        clientThread1.setName("clientThread 1");
        clientThread1.start();

        Thread clientThread2 = new Thread(new Client("������ 2", frontend, RequestType.TOPUP, 50L));
        clientThread2.setName("clientThread 2");
        clientThread2.start();

        Thread clientThread3 = new Thread(new Client("������ 3", frontend, RequestType.WITHDRAW, 800L));
        clientThread3.setName("clientThread 3");
        clientThread3.start();

        Thread clientThread4 = new Thread(new Client("������ 4", frontend, RequestType.WITHDRAW, 200L));
        clientThread4.setName("clientThread 1");
        clientThread4.start();


    }
}
