package ru.sav;

public class Backend {
    private long balance;

    Backend(Long balance) {
        this.balance = balance;
    }

    private void printBalance() {
        System.out.println("������: " + this.balance);
    }

    public synchronized void increaseBalance(String processorName, String clientName, Long amount) {
        try {
            balance += amount;
            System.out.println(processorName + ": ������ �� " + clientName + " ����������. ���������� �� " + amount + " ���.");
            printBalance();
        }
        catch (Exception e) {
            System.out.println(processorName + ": ������ �� " + clientName + " �� ����������. ");
            System.out.println(e.toString());
            printBalance();
        }
    }

    public synchronized void decreaseBalance(String processorName, String clientName, Long amount) {
        if ((balance - amount) < 0) {
            System.out.println(processorName + ": ������ �� " + clientName + " �� ������ " + amount + " �� ����������. ������������ �������");
        } else {
            balance -= amount;
            System.out.println(processorName + ": ������ �� " + clientName + " ����������. ������ " + amount + " ���.");
        }
        printBalance();
    }


}
