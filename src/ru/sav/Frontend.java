package ru.sav;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Frontend {
    private static final int MAX_REQUEST_COUNT = 2;

    Queue<Request> queue = new LinkedList<>();

    public synchronized void setRequest(Request req) {
        while (queue.size() >= MAX_REQUEST_COUNT) {
            try {
                wait();
            }
            catch (InterruptedException e) {
                System.out.println(e.toString());
            }
        }
        queue.add(req);
    }

    public synchronized Request getRequest() {
        Request req =  queue.poll();
        notify();
        return req;
    }

}
